-- Return Customer Name of customers who are from Ph
SELECT customerName FROM customers WHERE country = "Philippines";

-- lastname and firstname of customername La Rochelle Gifts
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- productName,MSRP from products whose name is The Titanic
SELECT productName,MSRP FROM products WHERE productName = "The Titanic";

-- firstname and lastname of employee whose email is jfirrelli@classicmodelcars.com
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- names of customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;

-- firstname lastname email of employee whose last name is patterson and firstname is steve
SELECT firstName,lastName,email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- customerName,country,credit limit of customers whose countries are NOT USE AND whose credit limits greater than 3000
SELECT customerName,country,creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- customer number of orders whose comments contain the string 'DHL'
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- product lines where text description mentions phrase 'state of the art'
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- countries of customers without duplication 
SELECT DISTINCT country FROM customers;

-- statuses of orders w/o duplication
SELECT DISTINCT status FROM orders;

-- customerName and country whose country is USA,France OR Canada
SELECT customerName,country FROM customers WHERE country IN ("USA","France","Canada");

-- Join OFFICES and EMPLOYEES and return the firstName,LastName and office's city of employees where there office is in Tokyo
SELECT firstName,lastName,city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE city = "Tokyo";

-- join customers and employees and return names of customers where the employee who served them is Leslie Thompson
SELECT customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE firstName = "Leslie" AND lastName = "Thompson";

-- productName and quantity in stock of products that belong to productline planes with stock quantities less than 1000
SELECT productName,quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;

-- customers name with phone number containing "+81"
SELECT customerName FROM customers WHERE phone LIKE "+81%";

-- number of customers in the UK
SELECT COUNT(country) FROM customers WHERE country = "UK";

-- Join tables return product names and customer name of products ordered by "Baane Mini Imports"
SELECT productName,customerName FROM orderdetails
	JOIN products ON orderdetails.productCode = products.productCode
	JOIN orders ON orderdetails.orderNumber = orders.orderNumber
	JOIN customers ON orders.customerNumber = customers.customerNumber
	WHERE customerName = "Baane Mini Imports";
